def add_numbers(num1, num2):
    """Function to add two numbers."""
    return num1 + num2

# Example usage:
if __name__ == "__main__":
    number1 = 5
    number2 = 10
    sum_result = add_numbers(number1, number2)
    print("The sum is:", sum_result)

    # Intentional bug 1: Passing a string instead of a number
    number3 = "15"
    sum_result = add_numbers(number1, number3)  
    print("The sum is:", sum_result)
